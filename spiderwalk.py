import collections
class Graph:
    def _init_(self, num_strands):
        self.num_strands = num_strands
        self.adj_list = [[] for _ in range(num_strands)]
    def add_bridge(self, strand1, strand2):
        self.adj_list[strand1].append(strand2)
        self.adj_list[strand2].append(strand1)
    def min_bridges(graph, start, target):    
        visited = [False] * graph.num_strands
        queue = collections.deque([start])
        visited[start] = True
        distance = [float('inf')] * graph.num_strands
        distance[start] = 0

    while queue:
        curr = queue.popleft()
        for neighbor in graph.adj_list[curr]:
            if not visited[neighbor]:
                visited[neighbor] = True
                distance[neighbor] = distance[curr] + 1
                queue.append(neighbor)
                if neighbor == target:
                    return distance[target]
    return -1
num_strands = 7
num_bridges = 5
spiderweb = Graph(num_strands)
for i in range(num_bridges):
    strand1, strand2 = map(int,input().split())
    spiderweb.add_bridge(int(strand1), int(strand2))
start_strand = int(input())
target_strand = int(input())
min_b = min_bridges(spiderweb, start_strand, target_strand)
if min_b != -1:
    print( min_b)
